/**
 * @file
 */

(function ($) {

  Drupal.didomi = Drupal.didomi || {};

  // Test if content from a vendor could be displayed. The vendor and all
  // associated purposes must be accepted.
  Drupal.didomi.canDisplayContent = function (vendorId) {
    vendor = Didomi.getVendorById(vendorId);
    var consent = Didomi.getUserConsentStatusForVendor(vendor.id);
    if (consent) {
      for (var i = 0; i < vendor.purposeIds.length; i++) {
        if (!Didomi.getUserConsentStatusForPurpose(vendor.purposeIds[i])) {
          consent = false;
          break;
        }
      }
    }
    return consent;
  };

  Drupal.didomi.engagementWidthCallback = function (element) {
    return '100%';
  };

  window.didomiOnReady = window.didomiOnReady || [];
  window.didomiOnReady.push(function (Didomi) {
    // I'm not sure if it is necessary. Did not test.
    if (!Didomi.isConsentRequired()) {
      return;
    }

    $('script[type="didomi/html"]').once('didomi-engagement', function () {
      var $this = $(this);
      var vendor = Didomi.getVendorById($this.attr('data-vendor'));

      if (Drupal.didomi.canDisplayContent(vendor.id)) {
        return;
      }

      var $div = $('<div class="didomi-engagement"><div class="didomi-engagement-inner"></div></div>');
      var html = '<div class="description">' + Drupal.theme('didomiEngagement', vendor) + '</div>';
      var $button = $('<button class="button">' + Drupal.t('Accept', {}, {context: 'Didomi engagement'}) + '</button>');
      $button.data('vendor', vendor.id);
      $button.click(function () {
        var vendor = Didomi.getVendorById($(this).data('vendor'));
        var transaction = Didomi.openTransaction();
        transaction.enableVendor(vendor.id);
        for (var i = 0; i < vendor.purposeIds.length; i++) {
          transaction.enablePurpose(vendor.purposeIds[i]);
        }
        transaction.commit();
      });
      var css = {
        'position': 'relative',
        'width': Drupal.didomi.engagementWidthCallback($this),
        'min-height': $this.css('height')
      }
      if (typeof Drupal.didomi.engagementHeightCallback === 'function') {
        if (height = Drupal.didomi.engagementHeightCallback($this)) {
          css['height'] = height;
        }
      }
      $div.css(css);
      $div.find('.didomi-engagement-inner')
          .html(html)
          .append($button);
      $this.after($div);
    });

    $('.didomi-preferences').click(function (event) {
      event.preventDefault();
      Didomi.preferences.show();
    });
  });

  window.didomiEventListeners = window.didomiEventListeners || [];
  window.didomiEventListeners.push({
    event: 'consent.changed',
    listener: function (context) {
      $('.didomi-engagement .button').each(function () {
        // Test again and remove consent is given.
        if (Drupal.didomi.canDisplayContent($(this).data('vendor'))) {
          $(this).parents('.didomi-engagement').remove();
        }
      });
    }
  });

  Drupal.theme.prototype.didomiEngagement = function(vendor) {
    var purposes = vendor.purposeIds.map(function (purposeId) {
      var name = Didomi.getPurposeById(purposeId).name;
      var lang = $('html').attr('lang');
      if (typeof name[lang] === 'undefined') {
        lang = Object.keys(name)[0];
      }
      return name[lang];
    });
    return Drupal.t('You must accept cookies from @vendor (@purposes) to view this content.', {
      '@vendor': vendor.name,
      '@purposes': purposes.join(', '),
    });
  }

})(jQuery);
