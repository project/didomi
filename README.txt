INTRODUCTION
------------

The Didomi module integrates Didomi's Consent Management Platform into Drupal.
It has built-in support for some modules and extensible for other modules.


REQUIREMENTS
------------

A valid Didomi API key. There is no free plan, but you might request a trial.


INSTALLATION
------------

Install as you would normally install a contributed Drupal. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.


CONFIGURATION
-------------

* Set the vendors variable in `settings.php`:

    $conf['didomi_vendor_mapping'] = [
      'c:dailymotio-HDNMAFkJ' => 'dailymotion',
      'didomi:youtube' => 'youtube',
      'c:google-XXXXX' => 'google_analytics'
    ];

  In the above example, support for YouTube, Dailymotion and Google Analytics is enabled.

* Set the public API key:

    $conf['didomi_api_key'] = 'fc2f6814-6deb-44fd-b0f0-05f56cea69b1';


CUSTOMIZATION
-------------

* Add more vendors

Use `hook_didomi_tags_alter` if you want to add purposes or to add more patterns
for vendors.

* Block content from custom modules

Currently this module supports a few Scald-related modules. Check
`didomi_ENTITY_TYPE_view()` for an inspiration of custom module support.

* Render of the engagement element

To customize the height of the engagement element, define a callback in your
JavaScript name `Drupal.didomi.engagementHeightCallback`. For example:

    Drupal.didomi = Drupal.didomi || {};
    Drupal.didomi.engagementHeightCallback = function (element) {
      if (!$(element).parent().hasClass('video')) {
        // If it is not a video, do not set a height.
        return false;
      }
      // The video has a 16:9 ratio, so match it.
      return $(element).width()*9/16;
    }

To customize the width of the engagement, override the width callback. It should
always return something. By default, it return the parent's width.

    Drupal.didomi.engagementWidthCallback = function (element) {
      return '100%';
    }

* Didomi preferences manager

To show the Didomi preferences manager, create a link or a button anywhere in
the page, with a class `didomi-preferences`.
